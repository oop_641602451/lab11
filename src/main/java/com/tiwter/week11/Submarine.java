package com.tiwter.week11;

public class Submarine extends Vahicle implements Swimable {

    public Submarine(String name, String engineName) {
        super(name, engineName);
    }
    @Override
    public String toString() {
        return "Submarine(" + this.getName() + ")" + " engine: " + this.geteEngineName();
    }
    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
        
    }
}
