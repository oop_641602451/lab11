package com.tiwter.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        System.out.println();

        Bird bird1 = new Bird("Jib");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        System.out.println();

        Fish fish1 = new Fish("Gold");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        System.out.println();

        Plane plane1 = new Plane("Boeing", "Boeing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();
        System.out.println();

        Dog dog1 = new Dog("dang");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        System.out.println();

        Cat cat1 = new Cat("Tom");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        System.out.println();

        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        System.out.println();

        Human human1 = new Human("Tiw");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();
        System.out.println();

        Snake snake1 = new Snake("Peter");
        snake1.eat();
        snake1.craw();
        snake1.sleep();
        System.out.println();

        Crocodile crocodile1 = new Crocodile("Jonh");
        crocodile1.eat();
        crocodile1.craw();
        crocodile1.swim();
        crocodile1.sleep();
        System.out.println();

        Submarine submarine1 = new Submarine("USS MAX", "USSMAX Engine");
        submarine1.swim();
        System.out.println();


        Flyable[] flyableObjects = {bat1, bird1, plane1};
        for(int i=0; i<flyableObjects.length; i++) {
            flyableObjects[i].takeoff();
            flyableObjects[i].fly();
            flyableObjects[i].landing();
        System.out.println();
        
        }
        Walkable[] walkableObjects = {dog1, cat1, rat1, human1};
        for(int j=0; j<walkableObjects.length; j++) {
            walkableObjects[j].walk();
            walkableObjects[j].run();
        System.out.println();
        }

        Crawable[] crawableObjects = {snake1, crocodile1};
        for(int q=0; q<crawableObjects.length; q++) {
            crawableObjects[q].craw();
        System.out.println();
        }

        Swimable[] swimableObjects = {fish1, crocodile1, submarine1};
        for(int p=0; p<crawableObjects.length; p++) {
            swimableObjects[p].swim();
        System.out.println();
        }
    }
}
